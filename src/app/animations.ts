import {
  trigger,
  animate,
  transition,
  style,
  query,
  group
} from '@angular/animations';

export const slideRight = trigger('fadeAnimation', [
  transition('* => *', [
    query(
      ':enter',
      [style({ transform: 'translate(0,0)' })],
      { optional: true }
    ),
    group([query(
      ':leave',
      [style({ transform: 'translate(0,0)' }), animate('3s', style({ transform: 'translate(100vw,0)' }))],
      { optional: true }
    ),
    query(
      ':enter',
      [style({ transform: 'translate(-100vw,0)' }), animate('3s', style({ transform: 'translate(0,0)' }))],
      { optional: true }
    )
    ])])
]);
