import { NgModule } from '@angular/core';
import { Routes, RouterModule, Router } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { AboutComponent } from './components/about/about.component';
import { Navigation1Component } from './components/navigation1/navigation1.component';


export const routes: Routes = [
 { path: 'home', component: HomeComponent },
  { path: 'about', component: AboutComponent },
  // { path: 'navigation1', component: Navigation1Component }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
