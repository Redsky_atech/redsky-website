import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
   MatTabsModule,
   MatButtonModule,
    MatNativeDateModule, 
    MatIconModule,
     MatSidenavModule,
      MatListModule,
       MatToolbarModule
} from '@angular/material';

const thirdPartyModules = [
  MatButtonModule,
  MatTabsModule,
  MatNativeDateModule, 
  MatIconModule,
   MatSidenavModule,
    MatListModule,
     MatToolbarModule
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ...thirdPartyModules
  ],
  exports: [...thirdPartyModules]
})
export class SharedModule { }
