import { Component, OnInit } from '@angular/core';
import { Platform } from '@angular/cdk/platform';
import { Observable, of, observable } from 'rxjs';
import { resolveReflectiveProviders } from '@angular/core/src/di/reflective_provider';
import { reject } from 'q';
import { resolve } from 'url';
import { error } from 'util';



@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  left = "unfocused";
  right = "unfocused";
  top = "unfocused";
  click: any;
  btn1: any;

  document: any;
  content: any;
  myScrollContainer: any;
  
  constructor() {
// ................................................subscribe using RxJS Library....................
  const  myObservable= of(1,2,3);

  const myObserver={
    next(n){},
    // {console.log(n);},
    error(e){},
    // {console.log(e);},
    complete(){}
    // {console.log('complete');}
  }

  // myObservable.subscribe(myObserver);
  // ........................................or...........................................
  myObservable.subscribe(
    n=>console.log('a:'+n),
    e=>console.log('b:'+e),
    ()=>console.log('c:completed')
    );


// .................................................................................................

// ...................................subscription using function...................................

// function seqSubscriber(observer){
//   observer.next(1);
//   observer.next(2);
//   observer.next(3);
//   observer.complete();

//   return {unsubscribe(){}};
// }
// const seq=new Observable(seqSubscriber);

// seq.subscribe({
//   next(n){console.log('return:'+n);},
//   complete(){console.log('complete');}

// })
// .................................................................................................


// .................................................delayed sequence................................

// function seqSubscriber1(observer){

// const seq=[1,2,3];
// let timeOutId;

// function doSeq(arr,idx){
//   timeOutId=setTimeout(()=>{
//     observer.next(arr[idx]);

//   if(idx==arr.length-1){
//     observer.complete();
//   }
//   else{
//       doSeq(arr,++idx);
//     }

//   },1000)
// }

// doSeq(seq,0);


// return {unsubscribe() {
//   clearTimeout(timeOutId);
// }};
// } 

// const sequence =new Observable(seqSubscriber1);

// sequence.subscribe({
//   next(n){console.log('1st:'+n)},
//   complete(){console.log('1st:is completed')}
// });


// setTimeout(() => {
//   sequence.subscribe({
//     next(n){console.log('2nd:'+n)},
//     complete(){console.log('2nd:is completed')}
//   });
// }, 500);

// .................................................................................................
// var promise = new Promise((resolve, reject) => {
//   setTimeout(() => {
//     console.log("Async Work Complete");
//     resolve();
//   }, 1000);
// });
// function notifyAll(fnSms, fnEmail) {   
//   console.log('starting notification process');   
//   fnSms();   
//   fnEmail();   
// }   
// notifyAll(function() {   
//   console.log("Sms send ..");   
// }, 
// function() {   
//   console.log("email send ..");   
// });   
// console.log("End of script"); 
  

// function notifyAll1(Sms, Email) {   
//   setTimeout(function() {   
//      console.log('starting notification process');   
//      Sms();   
//      Email();   
//   },4000);   
// }   
// notifyAll1(function() {   
//   console.log("Sms send ..");   
// },  
// function() {   
//   console.log("email send ..");   
// });   
// console.log("End of script");


// function notify(sms,email){
//   setTimeout(function(){
//     console.log("one");
//     sms();
//     email();
//     setTimeout(function(){
//       console.log("two");
//       sms();
//     email();
//       setTimeout(function(){
//         console.log("three");
//         sms();
//     email();
//       },3000);
//     },3000);
//   },3000);
// }
// notify(function(){
// console.log("send......");
// },
// function(){
//   console.log("email.....");
// });
// console.log("started");
// .................................................................................................

function getSum(x,y){
var isNegative=function(){
  return x<0|| y<0;
}
var promise= new Promise(function(resolve, reject){
  if(isNegative()){
    reject("error")
  }
  resolve(x+y);                       
});
return promise;
}

  getSum(4,5)
  .then(function(result){
    console.log(result);
    return getSum(-10,20)
  },
   function(result){
     console.log(error);
   })
  .then(function(result){
    console.log(result);
  },
  function(error){
    console.log(error);
  }
  );
  console.log("end");

// ..............................................................................




function notify2(sms,email){
  console.log("one");
    sms();
    email();
}
notify2(function(){
console.log("send......");
},
function(){
  console.log("email.....");
});
console.log("started");

// .................................................................................


function task(){
  var promise= new Promise((resolve,reject)=>{
    setTimeout
  })
}

}

openBox(){
  document.getElementById("myForm").style.display = "block";
}

 closeBox(){
  document.getElementById("myForm").style.display = "none";
}
ngOnInit() {
  }
  
}