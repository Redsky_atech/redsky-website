import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { ChangeDetectionStrategy } from '@angular/compiler/src/core';
import { trigger, transition, useAnimation } from '@angular/animations';
import { zoomOut } from 'ng-animate';
import { Router } from '@angular/router';
import * as $ from 'jquery';
import { BlockScrollStrategy } from 'igniteui-angular';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  // animations: [
  //   trigger('zoomOut', [transition('* => *', useAnimation(zoomOut))])
  // ],

})
export class HomeComponent {
  public imagesUrl;
  new: any;
  name: string = "";
  value: any;
  values = [];

 


  ngOnInit() {
    this.imagesUrl = [
      'http://comicsalliance.com/files/2011/04/strips02.jpg',
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSq4HTtZrfKqNo5riVYiOBBL7-9laaPZcW1RfDfYGvb6BezfMtQ',
      'https://s-media-cache-ak0.pinimg.com/originals/73/f3/08/73f30861d214eea1d6c5d99fe72b3053.jpg',
      'https://bmj2k.files.wordpress.com/2011/04/heroes.jpg',
      '../../assets/images/comp37.png',
      
    ];
  }
  
  send() {
    this.values.push(this.value)
    this.values.push()
  }
  onKey(event: any) {
    this.value = event.target.value
  }
  del= function(value){
    var index=this.values.indexOf(value);
  this.values.splice(index,1)
  }

  numbers: string[] = ['Welcome to Angular! Angular helps you', ' build modern applications for the web,', 'This guide shows', 'you how to build and run a simple Angular app. You', 'll use the Angular CLI tool to', ' accelerate development, while adhering to the Style Guide', ' recommendations that benefit every Angular project.',
    'Welcome to Angular! Angular helps you', ' build modern applications for the web,', 'This guide shows', 'you how to build and run a simple Angular app. You', 'll use the Angular CLI tool to', ' accelerate development, while adhering to the Style Guide', ' recommendations that benefit every Angular project.',
    'Welcome to Angular! Angular helps you', ' build modern applications for the web,'];
  constructor(private router: Router) {
    for (let index = 0; index < 3; index++) {
      this.numbers.push("");
    }

  }
  
  click(){
    $("a").on('click', function(event) {
      if (this.hash !== "") {
       event.preventDefault();
       var hash = this.hash;
         $('html, body').animate({
          scrollTop: $(hash).offset().top
        }, 1000, function(){
          window.location.hash = hash;
        });
      } 
    });
  };



  


  openBox(){
    document.getElementById("box").style.display = "block";
  }
  
   closeBox(){
    document.getElementById("box").style.display = "none";
  }


}


    

  