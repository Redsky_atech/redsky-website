import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ShowOnDirtyErrorStateMatcher } from '@angular/material';

@Component({
  selector: 'app-navigation1',
  templateUrl: './navigation1.component.html',
  
  styleUrls: ['./navigation1.component.css'],
})
export class Navigation1Component implements OnInit {
  public show: boolean = true;
  public iconName: any = 'menu';
  public l: string;
  public display1: any = 'Show';

  homeLine = "unfocused-line";
  aboutLine = "unfocused-line";
  contactLine = "unfocused-line";


  homeClicked: boolean = false;
  aboutClicked: boolean = false;
  contactClicked: boolean = false;

  home: string

  constructor() {
    this.home = "{color:pink;background-color:red}"
  }

  ngOnInit() {
  }
  toggle() {
    this.show = !this.show;

    // CHANGE THE NAME OF THE BUTTON.
    if (this.show)
      this.iconName = "menu";
    else
      this.iconName = "close";
  }

  onClicked(page: string) {

    console.log("Clicked")


    if (page == "Home") {
      this.homeLine = "focused-line"
      this.aboutLine = "unfocused-line"
      this.contactLine = "unfocused-line"
      this.homeClicked = true;
      this.aboutClicked = false;
       this.contactClicked = false;
    } else if (page == "About") {
      this.homeLine = "unfocused-line"
      this.aboutLine = "focused-line"
      this.contactLine = "unfocused-line"
      this.homeClicked = false;
      this.aboutClicked = true;
      this.contactClicked = false;
    } else if (page == "Contact") {
      this.homeLine = "unfocused-line"
      this.aboutLine = "unfocused-line"
      this.contactLine = "focused-line"
      this.homeClicked = false;
      this.aboutClicked = false;
      this.contactClicked = true;
    }
    else {
      this.homeLine = "unfocused-line"
      this.aboutLine = "unfocused-line"
      this.contactLine = "unfocused-line"
    }
  }

  mouseEnter(page: string) {
    if (page == "Home") {
      this.homeLine = "focused-line"
      this.aboutLine = "unfocused-line"
      this.contactLine = "unfocused-line"

    } else if (page == "About") {
      this.homeLine = "unfocused-line"
      this.aboutLine = "focused-line"
      this.contactLine = "unfocused-line"
    } else if (page == "Contact") {
      this.homeLine = "unfocused-line"
      this.aboutLine = "unfocused-line"
      this.contactLine = "focused-line"
    }
    else {
      this.homeLine = "unfocused-line"
      this.aboutLine = "unfocused-line"
      this.contactLine = "unfocused-line"
    }
  }

  mouseLeave(page: string) {
    if (this.homeClicked) {
      this.homeLine = "focused-line"
      this.aboutLine = "unfocused-line"
      this.contactLine = "unfocused-line"

    } else if (this.aboutClicked) {
      this.homeLine = "unfocused-line"
      this.aboutLine = "focused-line"
      this.contactLine = "unfocused-line"
    } else if (this.contactClicked) {
      this.homeLine = "unfocused-line"
      this.aboutLine = "unfocused-line"
      this.contactLine = "focused-line"
    }
    else{
      this.homeLine = "unfocused-line"
      this.aboutLine = "unfocused-line"
      this.contactLine = "unfocused-line"
    }

    // this.homeLine = "unfocused-line"
    // this.aboutLine = "unfocused-line"
    // this.contactLine = "unfocused-line"
  }

}
