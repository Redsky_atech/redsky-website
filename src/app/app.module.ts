import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {StickyModule} from 'ng2-sticky-kit';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from "@angular/router";
import { ScrollingModule } from '@angular/cdk/scrolling';
import { SliderModule } from 'angular-image-slider';
import { APP_BASE_HREF, CommonModule } from '@angular/common';
import { Navigation1Component } from './components/navigation1/navigation1.component';
import { AboutComponent } from './components/about/about.component';
import { HomeComponent } from './components/home/home.component';
import { SharedModule } from './shared/shared.module';

import { 
	IgxButtonModule,
	IgxIconModule,
	IgxNavigationDrawerModule,
	IgxRippleModule,
	IgxToggleModule
 } from "igniteui-angular";
 
// const appRoutes: Routes = [
//   { path: '', component: HomeComponent, data: { title: 'Home Component' } },
//   { path: 'home', component: HomeComponent, data: { title: 'Home Component' } },
//   { path: 'about', component: AboutComponent, data: { title: 'About Component' } },

// ];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    Navigation1Component
  ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ScrollingModule,
    SliderModule,
    IgxButtonModule,
	IgxIconModule,
	IgxNavigationDrawerModule,
	IgxRippleModule,
  IgxToggleModule,
  StickyModule,
  SharedModule
  ],

  providers: [{ provide: APP_BASE_HREF, useValue: '/' }],
  bootstrap: [AppComponent]
})
export class AppModule { }
