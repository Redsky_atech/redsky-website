import { Component, HostBinding, OnInit } from '@angular/core';
import { Router, RouterOutlet } from '@angular/router';
import { slideRight } from './animations';
import { Platform } from '@angular/cdk/platform';

import {
  trigger,
  state,
  style,
  animate,
  transition,
} from '@angular/animations';

// import { slideInAnimation } from './animation';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
   animations: [slideRight]
})

export class AppComponent implements OnInit {
  hide: boolean;
  ngOnInit(): void {
    setTimeout(() => {
      this.isRendering=true;
      this.stopRendering=false;
    }, 3000);

    // setTimeout(() => {
    //   this.stopRendering=false;
    // }, 3000);
  }

public show: boolean = true;
public iconName: any = 'menu';

  click: any;
  btn1: any;
  document: any;
  content: any;
  myScrollContainer: any;
  load: any;
  isRendering: boolean;
  stopRendering:boolean;

  constructor(private router: Router) {

    // this.router.navigate(['/home']);
    this.router.navigate(['/home'])

    const screenHeight = window.screen.height;
    
    window.onscroll = () => {
      this.scrollFunction();

    }

    this.isRendering = false;
    this.stopRendering=true;
  }
  toggle() {
    this.show = !this.show;

    // CHANGE THE NAME OF THE BUTTON.
    if (this.show)
      this.iconName = "menu";
    else
      this.iconName = "close";
  }


  // ................................go to top,bottom....................................
  scrollFunction() {
    if (document.body.scrollTop > window.screen.height * 1 || document.documentElement.scrollTop > window.screen.height * 1) {
      document.getElementById("btn1").style.display = "block";
      document.getElementById("btn2").style.display = "none";

    }
    else {
      document.getElementById("btn1").style.display = "none";
      document.getElementById("btn2").style.display = "block";

    }
  }
  top1() {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });

  }
  bottom1() {
    window.scroll({
      left: 0,
      top: document.body.scrollHeight || document.documentElement.scrollHeight,
      behavior: 'smooth'
    });
  }
  
}
// .....................................end...............................................









